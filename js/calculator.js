function calculateCaloricGoals() {
    var age = parseInt(document.getElementById('age').value);
    var gender = document.querySelector('input[name="gender"]:checked').value;
    var height = parseFloat(document.getElementById('height').value);
    var weight = parseFloat(document.getElementById('weight').value);
    var activity = parseFloat(document.getElementById('activity').value);

    var bmr;

    if (gender === 'male') {
        bmr = 10 * weight + 6.25 * height - 5 * age + 5;
    } else {
        bmr = 10 * weight + 6.25 * height - 5 * age - 161;
    }

    var tdee;

    switch (activity) {
        case 1.2:
            tdee = bmr * 1.2; // Sedentary
            break;
        case 1.375:
            tdee = bmr * 1.375; // Lightly Active
            break;
        case 1.55:
            tdee = bmr * 1.55; // Moderately Active
            break;
        case 1.725:
            tdee = bmr * 1.725; // Very Active
            break;
        case 1.9:
            tdee = bmr * 1.9; // Extremely Active
            break;
        default:
            tdee = bmr; // Default to BMR if activity level is not recognized
    }

    var resultBox = document.getElementById('resultBox');
    resultBox.innerHTML = `
        <p>Caloric Goal for Weight Loss: ${tdee - 500} calories/day</p>
        <p>Caloric Goal for Weight Maintenance: ${tdee} calories/day</p>
        <p>Caloric Goal for Weight Gain: ${tdee + 500} calories/day</p>
    `;
    resultBox.style.color="orange";
}