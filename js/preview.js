 // Get the image URL from the query parameter
 const queryString = window.location.search;
 const urlParams = new URLSearchParams(queryString);
 const imageUrl = urlParams.get('image');

 // Set the source of the preview image
 document.getElementById('previewImage').src = imageUrl;

 // Display the description based on the image URL
 switch (imageUrl) {
     case './images/arm1.png':
         document.getElementById('description').innerHTML = "Bicep curl: <br> A squat is a full-body exercise that involves bending the knees while keeping the back straight and lowering the body into a seated position and then returning to a standing position."+
         " <br>Stand with your feet shoulder-width apart or slightly wider."+
         "<br>Keep your back straight, chest up, and core engaged."+
         "<br>Initiate the movement by bending your hips and knees, as if you are sitting back into a chair."+
         "<br>Lower your body until your thighs are parallel to the ground or as far as your flexibility allows. Ensure your knees are aligned with your toes."+
         "<br> Press through your heels and extend your hips and knees to return to the starting position.";
         break;
     case './images/leg1.png':
     document.getElementById('description').innerHTML = "Squat: <br> bicep curl is a strength training exercise that involves flexing the elbow joint to bring the weight (usually a dumbbell or a barbell) toward the shoulder."+
         " <br>Stand with your feet shoulder-width apart and hold a dumbbell or barbell in your hands, with your palms facing forward."+
         "<br>Keep your upper arms stationary and exhale as you curl the weights while contracting your biceps."+
         "<br>Continue to raise the weights until your biceps are fully contracted and the weights are at shoulder level."+
         "<br>Inhale as you slowly begin to lower the dumbbells back to the starting position.";                   
          break;
     case './images/chest1.png':
     document.getElementById('description').innerHTML = "Chest row: <br> A chest supported row is an exercise that targets the muscles of the upper back, particularly the lats (latissimus dorsi), rhomboids, and traps. It is performed with the chest supported on a bench, allowing for a stable base and isolating the targeted muscles."+
         " <br>Lie face down on an incline bench with your chest and stomach pressing into the bench."+
         "<br>Hold a dumbbell in each hand, arms hanging straight down toward the floor."+
         "<br>Retract your shoulder blades and pull the dumbbells towards your hips, keeping your elbows close to your body."+
         "<br>Lower the weights back down with control.";                   
          break;
     case './images/abdomen1.png':
     document.getElementById('description').innerHTML = "Crunches: <br> Crunches are a strength-training exercise for the abdominal muscles. They involve lying on your back, bending your knees, and lifting your upper body toward your knees, emphasizing the contraction of the abdominal muscles."+
         " <br>Lie on your back on a mat with your knees bent and feet flat on the floor, hip-width apart."+
         "<br>Place your hands behind your head, but avoid pulling on your neck."+
         "<br>Engage your core muscles by drawing your belly button toward your spine."+
         "<br>Lift your head, neck, and shoulders off the mat, using your abdominal muscles."+
         "<br>Exhale as you crunch, bringing your chest towards your knees."+
         "<br>Inhale as you lower your upper body back to the starting position, keeping a controlled movement.";                   
          break;

     case './images/leg2.png':
     document.getElementById('description').innerHTML = "Split lunges: <br> Split lunges, also known as split squats, are a type of unilateral (single-leg) exercise that involves taking a step forward or backward into a lunge position and then lowering the body into a squat."+
         " <br>Start by standing with your feet together."+
         "<br>Take a step forward with one foot, creating a wide stance."+
         "<br>Lower your body by bending both knees, keeping your torso upright."+
         "<br>The back knee should hover just above the ground, and the front knee should be directly above the ankle.";                   
          break;
     
     case './images/abdomen2.png':
     document.getElementById('description').innerHTML = "Flutter kick: <br> Flutter kicks are a type of exercise that targets the muscles in the lower abdominal region and can be commonly found in workouts aimed at strengthening the core."+
         " <br>Lie on your back on a mat with your legs fully extended and your arms either by your sides or under your glutes for added support."+
         "<br>Lift your legs a few inches off the ground, keeping them straight. Engage your core muscles to maintain stability."+
         "<br>Begin fluttering your legs up and down in a quick, alternating motion. The movement is similar to the fluttering of butterfly wings."+
         "<br>Keep your lower back pressed into the mat to ensure that your core is engaged."+
         "<br>Focus on controlled, small movements to avoid excessive strain on the lower back."+
         "<br>Maintain steady and controlled breathing throughout the exercise. You can synchronize your breath with the fluttering motion."+
         "<br>Flutter kicks are often performed for a specific duration, such as 30 seconds to 1 minute, as part of a larger workout routine.";                   
          break;
    case './images/arm2.png':
    document.getElementById('description').innerHTML = "Hammer curl: <br> Hammer curls are a variation of bicep curls, a resistance exercise that targets the muscles of the upper arm."+
         " <br>Stand with your feet shoulder-width apart and hold a dumbbell in each hand, with your palms facing your torso. This is the neutral or hammer grip."+
        "<br>Keep your upper arms stationary and your elbows close to your torso."+
         "<br>Exhale and curl the weights while keeping your palms facing your torso. Continue to raise the weights until your biceps are fully contracted."+
        "<br>Inhale and slowly begin to lower the dumbbells back to the starting position."+
        "<br>Maintain a straight back and engage your core muscles."+
        "<br>Focus on controlled movements to isolate the biceps and prevent using momentum."+
        "<br>Keep your wrists straight throughout the exercise.";                   
         break;       
     
     // Add more cases for additional exercises
 }