function showAllExerciseContent(clickedOption) {
    // Show all exercise content
    document.querySelectorAll('.exerciseContent').forEach(function (exercise) {
        exercise.style.display = 'flex';
    });

    // Reset background color for all options
    document.querySelectorAll('.options').forEach(function (option) {
        option.style.backgroundColor = '';
    });

    // Set background color for the clicked option
    clickedOption.style.backgroundColor = 'orange';
}

function showExerciseContent(option, clickedOption) {
    // Hide all exercise content
    document.querySelectorAll('.exerciseContent').forEach(function (exercise) {
        exercise.style.display = 'none';
    });

    // Show selected exercise content
    if (option === 'option1') {
        document.getElementById('exerciseContent1').style.display = 'flex';
        document.getElementById('exerciseContent7').style.display = 'flex';
    } else if (option === 'option2') {
        document.getElementById('exerciseContent4').style.display = 'flex';
        document.getElementById('exerciseContent6').style.display = 'flex';
    } else if (option === 'option3') {
        document.getElementById('exerciseContent2').style.display = 'flex';
        document.getElementById('exerciseContent5').style.display = 'flex';
    } else if (option === 'option4') {
        document.getElementById('exerciseContent3').style.display = 'flex';
    }

    // Reset background color for all options
    document.querySelectorAll('.options').forEach(function (option) {
        option.style.backgroundColor = '';
    });

    // Set background color for the clicked option
    clickedOption.style.backgroundColor = 'orange';
}

// Initially show all exercise content
showAllExerciseContent(document.querySelector('.options:first-child'));