window.addEventListener('scroll', function() {
    var leftDiv = document.getElementById('leftDiv');
    var footer = document.querySelector('footer');
    
    // Calculate the distance between the bottom of the left div and the top of the footer
    var distance = footer.getBoundingClientRect().top - leftDiv.getBoundingClientRect().bottom;
  
    // Adjust the max-height of the left div based on the distance
    leftDiv.style.maxHeight = distance + 'px';
  });